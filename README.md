# ansible-best-practices

## Testing

### Gitlab CI
```yml
test:
  image: code.europa.eu:4567/digit-c4/dev/ansible-best-practices/ansible-test:python3.8-alpine
  tags:
    - docker
    - lab
  script:
    - docker-compose --file /roles/my_role/tests/docker-compose.yml up -d 
    - ansible-playbook /roles/my_role/tests/playbook.yml
```
